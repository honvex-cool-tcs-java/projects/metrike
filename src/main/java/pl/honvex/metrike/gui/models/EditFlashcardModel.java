package pl.honvex.metrike.gui.models;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.core.courses.Course;
import pl.honvex.metrike.core.learning.Flashcard;

public class EditFlashcardModel {
    public final Flashcard flashcard;
    public final Course course;
    public final Library library;
    public final StringProperty wordError = new ReadOnlyStringWrapper();
    public final StringProperty translationError = new ReadOnlyStringWrapper();
    public final ObservableList<String> translationList;
    public final boolean cancellable;

    public EditFlashcardModel(
        final Flashcard flashcard,
        final Course course,
        final Library library,
        final boolean cancellable
    ) {
        this.flashcard = flashcard;
        this.course = course;
        this.library = library;
        this.cancellable = cancellable;
        translationList = FXCollections.observableList(flashcard.getTranslations());
    }

    public void updateWord(final String word) {
        flashcard.setWord(word);
    }

    public void setWordError(final String message) {
        wordError.set(message);
    }

    public void setTranslationError(final String message) {
        translationError.set(message);
    }

    public void removeFlashcard() {
        course.getFlashcards().remove(flashcard);
    }
}
