package pl.honvex.metrike.gui.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Paint;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.core.courses.Lesson;
import pl.honvex.metrike.gui.utils.PaintUtils;

public class LessonModel {
    public final Library library;
    private final Lesson lesson;
    private Lesson.Question currentQuestion = null;

    public final StringProperty wordToTranslate = new ReadOnlyStringWrapper();
    public final StringProperty gradeDescription = new ReadOnlyStringWrapper();
    public final ObjectProperty<Paint> gradeColor = new ReadOnlyObjectWrapper<>();
    public final StringProperty remarksText = new ReadOnlyStringWrapper();
    public final StringProperty status = new ReadOnlyStringWrapper();
    public final StringProperty score = new ReadOnlyStringWrapper();
    public final BooleanProperty quizzing = new ReadOnlyBooleanWrapper(true);
    public final BooleanProperty inProgress = new ReadOnlyBooleanWrapper(true);

    public LessonModel(final Lesson lesson, final Library library) {
        this.lesson = lesson;
        this.library = library;
        update();
        updateScore();
    }

    public void answerQuestion(final String answer) {
        quizzing.set(false);
        final var grade = currentQuestion.answer(answer);
        final var quality = grade.quality();
        gradeColor.set(PaintUtils.qualityToPaint(quality));
        gradeDescription.set(quality.toString().replace("_", " ") + "!");
        remarksText.set(grade.remarks());
        updateScore();
    }

    public void update() {
        if(lesson.isInProgress()) {
            currentQuestion = lesson.nextQuestion();
            wordToTranslate.set(currentQuestion.get());
            quizzing.set(true);
            status.set("Lesson in progress: " + currentQuestion.number + " / " + lesson.getQuestionCount());
        } else {
            inProgress.set(false);
            wordToTranslate.set(null);
            quizzing.set(false);
            status.set("Lesson finished");
        }
        gradeDescription.set(null);
        remarksText.set(null);
    }

    private void updateScore() {
        score.set("Score: " + lesson.getScore());
    }

    public boolean answerIsRelevant(final String answer) {
        return answer != null && !answer.trim().isEmpty();
    }
}
