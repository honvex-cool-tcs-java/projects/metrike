package pl.honvex.metrike.gui.models;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.core.courses.Course;

public class BrowseCoursesModel {
    public final Library library;
    public final ObservableList<Course> courseList;
    public final StringProperty error = new ReadOnlyStringWrapper();

    public BrowseCoursesModel(final Library library) {
        this.library = library;
        courseList = FXCollections.observableList(this.library.getCourses());
    }

    public void setError(final String message) {
        error.set(message);
    }
}
