package pl.honvex.metrike.gui.models;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.honvex.metrike.Config;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.core.courses.Course;
import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.core.learning.judges.Judge;

public class EditCourseModel {

    public final Course course;
    public final Library library;
    public final boolean cancellable;
    public final ObservableList<Flashcard> flashcardList;
    public final StringProperty courseName = new SimpleStringProperty();
    public final ObservableList<Judge> judgeList;
    public final ObjectProperty<Judge> judge = new SimpleObjectProperty<>();
    public StringProperty nameError = new ReadOnlyStringWrapper();

    public EditCourseModel(final Course course, final Library library, final boolean cancellable) {
        this.course = course;
        this.library = library;
        this.cancellable = cancellable;
        flashcardList = FXCollections.observableList(this.course.getFlashcards());
        judgeList = FXCollections.observableList(Config.AVAILABLE_JUDGES);
        courseName.set(course.getName());
    }

    public void removeCourse() {
        library.getCourses().remove(course);
    }

    public void setError(final String message) {
        nameError.set(message);
    }

    public void update() {
        course.setName(courseName.get());
        course.setJudge(judge.get());
    }
}
