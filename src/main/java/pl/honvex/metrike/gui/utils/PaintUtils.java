package pl.honvex.metrike.gui.utils;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import pl.honvex.metrike.core.learning.grades.Quality;

public final class PaintUtils {
    private PaintUtils() {
    }

    public static Paint qualityToPaint(final Quality quality) {
        return switch(quality) {
            case PERFECT -> Color.GREEN;
            case INCORRECT_CAPITALIZATION -> Color.ORANGE;
            default -> Color.RED;
        };
    }
}
