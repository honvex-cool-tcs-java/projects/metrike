package pl.honvex.metrike.gui.utils;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;

public final class StageUtils {
    private StageUtils() {
    }

    public static Stage getStage(final ActionEvent event) {
        return (Stage)(((Node)event.getSource()).getScene().getWindow());
    }
}
