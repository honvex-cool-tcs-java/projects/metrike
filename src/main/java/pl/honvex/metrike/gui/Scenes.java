package pl.honvex.metrike.gui;

import javafx.scene.Scene;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.core.courses.Course;
import pl.honvex.metrike.core.courses.Lesson;
import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.gui.controllers.BrowseCoursesController;
import pl.honvex.metrike.gui.controllers.EditCourseController;
import pl.honvex.metrike.gui.controllers.EditFlashcardController;
import pl.honvex.metrike.gui.controllers.LessonController;
import pl.honvex.metrike.gui.models.BrowseCoursesModel;
import pl.honvex.metrike.gui.models.EditCourseModel;
import pl.honvex.metrike.gui.models.EditFlashcardModel;
import pl.honvex.metrike.gui.models.LessonModel;
import pl.honvex.metrike.gui.views.BrowseCoursesView;
import pl.honvex.metrike.gui.views.EditCourseView;
import pl.honvex.metrike.gui.views.EditFlashcardView;
import pl.honvex.metrike.gui.views.LessonView;

public final class Scenes {
    private Scenes() {
    }

    public static Scene lessonScene(final Lesson lesson, final Library library) {
        final var model = new LessonModel(lesson, library);
        final var controller = new LessonController(model);
        return new Scene(new LessonView(model, controller));
    }

    public static Scene browseCoursesScene(final Library library) {
        final var model = new BrowseCoursesModel(library);
        final var controller = new BrowseCoursesController(model);
        return new Scene(new BrowseCoursesView(model, controller));
    }

    public static Scene editCourseScene(final Course course, final Library library) {
        return editCourseScene(course, false, library);
    }

    public static Scene editCourseScene(
        final Course course,
        final boolean cancellable,
        final Library library
    ) {
        final var model = new EditCourseModel(course, library, cancellable);
        final var controller = new EditCourseController(model);
        return new Scene(new EditCourseView(model, controller));
    }

    public static Scene editFlashcardScene(
        final Flashcard flashcard,
        final boolean cancellable,
        final Course course,
        final Library library
    ) {
        final var model = new EditFlashcardModel(flashcard, course, library, cancellable);
        final var controller = new EditFlashcardController(model);
        return new Scene(new EditFlashcardView(model, controller));
    }
}
