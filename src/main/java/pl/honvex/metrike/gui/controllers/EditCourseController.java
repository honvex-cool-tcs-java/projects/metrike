package pl.honvex.metrike.gui.controllers;

import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.core.utils.TextUtils;
import pl.honvex.metrike.gui.Scenes;
import pl.honvex.metrike.gui.models.EditCourseModel;

public class EditCourseController {

    private final EditCourseModel model;

    public EditCourseController(final EditCourseModel model) {
        this.model = model;
    }

    private void checkedSetScene(final Stage stage, final Scene scene) {
        if(TextUtils.isEffectivelyEmpty(model.courseName.get()))
            model.setError("Invalid course name");
        else {
            model.setError(null);
            model.update();
            stage.setScene(scene);
        }
    }

    public void doneAction(final Stage stage) {
        checkedSetScene(stage, Scenes.browseCoursesScene(model.library));
    }

    public void deleteAction(final Flashcard selectedFlashcard) {
        model.flashcardList.remove(selectedFlashcard);
    }

    public void editAction(
        final Flashcard flashcard,
        final boolean cancellable,
        final Stage stage
    ) {
        checkedSetScene(
            stage,
            Scenes.editFlashcardScene(flashcard, cancellable, model.course, model.library)
        );
    }

    public void editAction(final Flashcard flashcard, final Stage stage) {
        editAction(flashcard, false, stage);
    }

    public void newAction(final Stage stage) {
        final var flashcard = new Flashcard();
        model.flashcardList.add(flashcard);
        editAction(flashcard, true, stage);
    }

    public void cancelAction(final Stage stage) {
        model.removeCourse();
        stage.setScene(Scenes.browseCoursesScene(model.library));
    }
}
