package pl.honvex.metrike.gui.controllers;

import javafx.stage.Stage;
import pl.honvex.metrike.core.utils.TextUtils;
import pl.honvex.metrike.gui.Scenes;
import pl.honvex.metrike.gui.models.EditFlashcardModel;

public class EditFlashcardController {
    private final EditFlashcardModel model;

    public EditFlashcardController(final EditFlashcardModel model) {
        this.model = model;
    }

    public void doneAction(final String word, final Stage stage) {
        if(TextUtils.isEffectivelyEmpty(word))
            model.setWordError("Invalid word");
        else if(model.translationList.isEmpty())
            model.setWordError("Please provide at least one translation");
        else {
            model.setWordError(null);
            model.updateWord(word);
            stage.setScene(Scenes.editCourseScene(model.course, model.library));
        }
    }

    public boolean addAction(final String newTranslation) {
        if(TextUtils.isEffectivelyEmpty(newTranslation)) {
            model.setTranslationError("Invalid translation");
            return false;
        } else {
            model.setTranslationError(null);
            model.translationList.add(newTranslation);
            return true;
        }
    }

    public void deleteAction(final String selectedItem) {
        model.translationList.remove(selectedItem);
    }

    public void cancelAction(final Stage stage) {
        model.removeFlashcard();
        stage.setScene(Scenes.editCourseScene(model.course, model.library));
    }
}
