package pl.honvex.metrike.gui.controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.honvex.metrike.core.courses.Course;
import pl.honvex.metrike.gui.Scenes;
import pl.honvex.metrike.gui.models.BrowseCoursesModel;

import java.io.IOException;

public class BrowseCoursesController {
    private final BrowseCoursesModel model;

    public BrowseCoursesController(final BrowseCoursesModel model) {
        this.model = model;
    }

    public void practiceAction(final Stage stage, final Course selectedCourse) {
        if(selectedCourse.isEmpty()) {
            model.setError("This course is empty! Try adding some flashcards");
        } else {
            model.setError("");
            final var lesson = selectedCourse.nextLesson(10);
            final var scene = Scenes.lessonScene(lesson, model.library);
            stage.setScene(scene);
        }
    }

    public void editAction(final Stage stage, final Course course, final boolean cancellable) {
        final var scene = Scenes.editCourseScene(course, cancellable, model.library);
        stage.setScene(scene);
    }

    public void editAction(final Stage stage, final Course course) {
        editAction(stage, course, false);
    }

    public void deleteAction(final Course selectedCourse) {
        final var question
            = "Do you really want to delete this course: \""
            + selectedCourse.getName()
            + "\"?";
        final var alert = new Alert(
            Alert.AlertType.CONFIRMATION,
            question,
            ButtonType.YES,
            ButtonType.NO
        );
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.showAndWait();
        if(alert.getResult() == ButtonType.YES)
            model.courseList.remove(selectedCourse);
    }

    public void newAction(final Stage stage) {
        final var course = new Course();
        model.courseList.add(course);
        editAction(stage, course, true);
    }

    public void loadAction(final Stage stage) {
        final var chooser = new FileChooser();
        chooser.setTitle("Choose a dictionary file");
        chooser.setSelectedExtensionFilter(
            new FileChooser.ExtensionFilter("Dictionary files", "*.dict")
        );
        final var file = chooser.showOpenDialog(stage);
        if(file == null) {
            model.setError("Please select a file");
            return;
        }
        try {
            final var course = Course.fromFile(file);
            model.courseList.add(course);
            model.setError(null);
        } catch(final IOException ioException) {
            model.setError(ioException.getMessage());
        } catch(final Throwable ignored) {
            model.setError("Sorry, an error has occurred");
        }
    }
}
