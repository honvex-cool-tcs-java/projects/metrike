package pl.honvex.metrike.gui.controllers;

import javafx.stage.Stage;
import pl.honvex.metrike.gui.Scenes;
import pl.honvex.metrike.gui.models.LessonModel;

public class LessonController {
    private final LessonModel model;

    public LessonController(final LessonModel model) {
        this.model = model;
    }

    public void submitAction(final String answer) {
        if(model.answerIsRelevant(answer))
            model.answerQuestion(answer);
    }

    public void continueAction() {
        model.update();
    }

    public void finishAction(final Stage stage) {
        stage.setScene(Scenes.browseCoursesScene(model.library));
    }
}
