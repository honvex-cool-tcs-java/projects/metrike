package pl.honvex.metrike.gui.views;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.core.learning.judges.Judge;
import pl.honvex.metrike.gui.controllers.EditCourseController;
import pl.honvex.metrike.gui.models.EditCourseModel;
import pl.honvex.metrike.gui.utils.StageUtils;

public class EditCourseView extends VBox {
    private final EditCourseModel model;
    private final EditCourseController controller;

    private final Label flashcardLabel = new Label("Flashcards:");
    private final ListView<Flashcard> flashcardListView = new ListView<>();
    private final VBox flashcardPanel = new VBox(flashcardLabel, flashcardListView);
    private final Button doneButton = new Button("Done");
    private final Button cancelButton = new Button("Cancel");
    private final Label nameLabel = new Label("Course:");
    private final TextField nameTextField = new TextField();
    private final HBox nameControl = new HBox(nameLabel, nameTextField);
    private final Text nameErrorText = new Text();
    private final Label judgeLabel = new Label("Judge:");
    private final ComboBox<Judge> judgeComboBox = new ComboBox<>();
    private final HBox judgeControl = new HBox(judgeLabel, judgeComboBox);
    private final VBox metaPanel = new VBox(nameControl, judgeControl, nameErrorText);
    private final Button editButton = new Button("Edit");
    private final Button deleteButton = new Button("Delete");
    private final Button newButton = new Button("New");

    private final HBox buttonPanel = new HBox(editButton, deleteButton, newButton);

    public EditCourseView(final EditCourseModel model, final EditCourseController controller) {
        this.model = model;
        this.controller = controller;
        flashcardListView.setItems(this.model.flashcardList);
        judgeComboBox.setItems(this.model.judgeList);
        judgeComboBox.getSelectionModel().select(this.model.course.getJudge());
        final var converter = new FlashcardStringConverter();
        nameTextField.setText(this.model.course.getName());
        flashcardListView.setCellFactory(
            ignored -> new TextFieldListCell<>(converter)
        );
        addChildren();
        bindProperties();
        registerActions();
        prettify();
    }

    private void prettify() {
        setAlignment(Pos.CENTER);
        setPadding(new Insets(15));
        setSpacing(15);
        nameTextField.setFont(new Font(20));
        nameLabel.setFont(new Font(20));
        nameControl.setAlignment(Pos.CENTER);
        nameControl.setSpacing(15);
        judgeControl.setAlignment(Pos.CENTER);
        judgeControl.setSpacing(15);
        metaPanel.setAlignment(Pos.CENTER);
        metaPanel.setSpacing(15);
        buttonPanel.setAlignment(Pos.CENTER);
        buttonPanel.setSpacing(10);
        nameErrorText.setFill(Color.RED);
    }

    private void registerActions() {
        editButton.setOnAction(this::editButtonOnAction);
        deleteButton.setOnAction(this::deleteButtonOnAction);
        newButton.setOnAction(this::newButtonOnAction);
        doneButton.setOnAction(this::doneButtonOnAction);
        cancelButton.setOnAction(this::cancelButtonOnAction);
    }

    private void cancelButtonOnAction(final ActionEvent actionEvent) {
        controller.cancelAction(StageUtils.getStage(actionEvent));
    }

    private void newButtonOnAction(final ActionEvent actionEvent) {
        controller.newAction(StageUtils.getStage(actionEvent));
    }

    private void editButtonOnAction(final ActionEvent actionEvent) {
        controller.editAction(getSelectedFlashcard(), StageUtils.getStage(actionEvent));
    }

    private Flashcard getSelectedFlashcard() {
        return flashcardListView.getSelectionModel().getSelectedItem();
    }

    private void doneButtonOnAction(final ActionEvent actionEvent) {
        controller.doneAction(StageUtils.getStage(actionEvent));
    }

    private void deleteButtonOnAction(final ActionEvent ignored) {
        controller.deleteAction(getSelectedFlashcard());
    }

    private void bindProperties() {
        final var notSelected = flashcardListView
            .getSelectionModel()
            .selectedItemProperty()
            .isNull();
        editButton.disableProperty().bind(notSelected);
        deleteButton.disableProperty().bind(notSelected);
        cancelButton.setVisible(model.cancellable);
        nameTextField.textProperty().bindBidirectional(model.courseName);
        nameErrorText.textProperty().bind(model.nameError);
        model.judge.bind(judgeComboBox.valueProperty());
    }

    private void addChildren() {
        getChildren().addAll(
            metaPanel,
            flashcardPanel,
            buttonPanel,
            doneButton,
            cancelButton
        );
    }

    private static class FlashcardStringConverter extends StringConverter<Flashcard> {
        @Override
        public String toString(final Flashcard flashcard) {
            final var translations = String.join(" / ", flashcard.getTranslations());
            return flashcard.getWord() + " (" + translations + ")";
        }

        @Override
        public Flashcard fromString(final String ignored) {
            return null;
        }
    }
}
