package pl.honvex.metrike.gui.views;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import pl.honvex.metrike.gui.controllers.EditFlashcardController;
import pl.honvex.metrike.gui.models.EditFlashcardModel;
import pl.honvex.metrike.gui.utils.StageUtils;

public class EditFlashcardView extends VBox {
    private final EditFlashcardModel model;
    private final EditFlashcardController controller;

    private final Button doneButton = new Button("Done");
    private final Button cancelButton = new Button("Cancel");
    private final Label wordLabel = new Label("Word:");
    private final TextField wordTextField = new TextField();
    private final Text wordErrorText = new Text();
    private final HBox wordControl = new HBox(wordLabel, wordTextField);
    private final VBox wordPanel = new VBox(wordControl, wordErrorText);
    private final Label translationLabel = new Label("Translations:");
    private final ListView<String> translationListView = new ListView<>();
    private final VBox translationPanel = new VBox(translationLabel, translationListView);
    private final Label newTranslationLabel = new Label("New translation:");
    private final TextField newTranslationTextField = new TextField();
    private final Button addButton = new Button("Add");
    private final Button deleteButton = new Button("Delete");
    private final Text translationErrorText = new Text();
    private final HBox controlPanel = new HBox(
        newTranslationLabel,
        newTranslationTextField,
        addButton,
        deleteButton,
        translationErrorText
    );

    public EditFlashcardView(
        final EditFlashcardModel model,
        final EditFlashcardController controller
    ) {
        this.model = model;
        this.controller = controller;
        cancelButton.setVisible(this.model.cancellable);
        addChildren();
        setData();
        bindProperties();
        registerActions();
        prettify();
    }

    private void prettify() {
        wordLabel.setFont(new Font(20));
        wordTextField.setFont(new Font(20));
        wordErrorText.setFill(Color.RED);
        translationErrorText.setFill(Color.RED);
        setAlignment(Pos.CENTER);
        setPadding(new Insets(15));
        setSpacing(15);
        translationListView.setPrefHeight(150);
        wordControl.setAlignment(Pos.CENTER);
        wordControl.setSpacing(10);
        wordPanel.setAlignment(Pos.CENTER);
        wordControl.setSpacing(10);
        controlPanel.setAlignment(Pos.CENTER);
        controlPanel.setSpacing(10);
    }

    private void bindProperties() {
        deleteButton.disableProperty().bind(
            translationListView.getSelectionModel().selectedItemProperty().isNull()
        );
        wordErrorText.textProperty().bind(model.wordError);
        translationErrorText.textProperty().bind(model.translationError);
        addButton.disableProperty().bind(newTranslationTextField.textProperty().isEmpty());
        addButton.setDefaultButton(true);
    }

    private void registerActions() {
        doneButton.setOnAction(this::doneButtonOnAction);
        addButton.setOnAction(this::addButtonOnAction);
        deleteButton.setOnAction(this::deleteButtonOnAction);
        cancelButton.setOnAction(this::cancelButtonOnAction);
    }

    private void cancelButtonOnAction(final ActionEvent actionEvent) {
        controller.cancelAction(StageUtils.getStage(actionEvent));
    }

    private void deleteButtonOnAction(final ActionEvent ignored) {
        controller.deleteAction(translationListView.getSelectionModel().getSelectedItem());
    }

    private void addButtonOnAction(final ActionEvent ignored) {
        if(controller.addAction(newTranslationTextField.getText()))
            newTranslationTextField.clear();
    }

    private void doneButtonOnAction(final ActionEvent actionEvent) {
        controller.doneAction(wordTextField.getText(), StageUtils.getStage(actionEvent));
    }

    private void setData() {
        wordTextField.setText(this.model.flashcard.getWord());
        translationListView.setItems(model.translationList);
    }

    private void addChildren() {
        getChildren().addAll(
            wordPanel,
            controlPanel,
            translationPanel,
            doneButton,
            cancelButton
        );
    }
}
