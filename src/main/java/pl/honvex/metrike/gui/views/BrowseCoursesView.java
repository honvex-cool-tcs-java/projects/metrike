package pl.honvex.metrike.gui.views;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import pl.honvex.metrike.core.courses.Course;
import pl.honvex.metrike.gui.controllers.BrowseCoursesController;
import pl.honvex.metrike.gui.models.BrowseCoursesModel;
import pl.honvex.metrike.gui.utils.StageUtils;

public class BrowseCoursesView extends VBox {
    private final BrowseCoursesModel model;
    private final BrowseCoursesController controller;

    private final Text applicationNameText = new Text("Μητρική - a flashcard app");
    private final Text headerText = new Text("My courses");
    private final Button practiceButton = new Button("Practice");
    private final Button editButton = new Button("Edit");
    private final Button deleteButton = new Button("Delete");
    private final Button newButton = new Button("New");
    private final Button loadButton = new Button("Load");
    private final Text errorText = new Text("");
    private final HBox buttonPanel = new HBox(
        editButton,
        deleteButton,
        newButton,
        loadButton
    );
    private final VBox controlPanel = new VBox(buttonPanel, errorText);
    private final ListView<Course> courseListView;

    public BrowseCoursesView(
        final BrowseCoursesModel model,
        final BrowseCoursesController controller
    ) {
        this.model = model;
        this.controller = controller;
        courseListView = new ListView<>(this.model.courseList);
        addChildren();
        bindProperties();
        registerActions();
        prettify();
    }

    private void addChildren() {
        getChildren().addAll(
            applicationNameText,
            headerText,
            courseListView,
            practiceButton,
            controlPanel
        );
    }

    private void bindProperties() {
        final var hasSelection = courseListView
            .getSelectionModel()
            .selectedItemProperty()
            .isNull();
        practiceButton.setDefaultButton(true);
        practiceButton.disableProperty().bind(hasSelection);
        editButton.disableProperty().bind(hasSelection);
        deleteButton.disableProperty().bind(hasSelection);
        errorText.textProperty().bind(model.error);
    }

    private void registerActions() {
        practiceButton.setOnAction(this::practiceButtonOnAction);
        editButton.setOnAction(this::editButtonOnAction);
        deleteButton.setOnAction(this::deleteButtonOnAction);
        newButton.setOnAction(this::newButtonOnAction);
        loadButton.setOnAction(this::loadButtonOnAction);
    }

    private void loadButtonOnAction(final ActionEvent actionEvent) {
        controller.loadAction(StageUtils.getStage(actionEvent));
    }

    private void newButtonOnAction(final ActionEvent actionEvent) {
        controller.newAction(StageUtils.getStage(actionEvent));
    }

    private void deleteButtonOnAction(final ActionEvent ignored) {
        controller.deleteAction(getSelectedCourse());
    }

    private void editButtonOnAction(final ActionEvent actionEvent) {
        controller.editAction(StageUtils.getStage(actionEvent), getSelectedCourse());
    }

    private void practiceButtonOnAction(final ActionEvent actionEvent) {
        controller.practiceAction(StageUtils.getStage(actionEvent), getSelectedCourse());
    }

    private Course getSelectedCourse() {
        return courseListView.getSelectionModel().getSelectedItem();
    }

    private void prettify() {
        headerText.setFont(new Font(20));
        errorText.setFill(Color.RED);
        setAlignment(Pos.CENTER);
        setSpacing(15);
        setPadding(new Insets(15));
        practiceButton.setFont(new Font(15));
        buttonPanel.setAlignment(Pos.CENTER);
        buttonPanel.setSpacing(10);
        controlPanel.setAlignment(Pos.CENTER);
        controlPanel.setSpacing(15);
        applicationNameText.setFont(new Font(40));
        applicationNameText.setFill(Color.NAVY);
    }
}
