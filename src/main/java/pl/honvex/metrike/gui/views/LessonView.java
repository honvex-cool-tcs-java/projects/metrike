package pl.honvex.metrike.gui.views;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import pl.honvex.metrike.gui.controllers.LessonController;
import pl.honvex.metrike.gui.models.LessonModel;
import pl.honvex.metrike.gui.utils.StageUtils;

public class LessonView extends VBox {
    private final LessonModel model;
    private final LessonController controller;

    private final Text statusText = new Text();
    private final Text scoreText = new Text();
    private final Label wordLabel = new Label();
    private final TextField answerField = new TextField();
    private final Text gradeText = new Text();
    private final Text remarksText = new Text();
    private final Button submitButton = new Button("Submit");
    private final Button continueButton = new Button("Continue");
    private final Button finishButton = new Button("Finish");

    private final HBox buttonPanel = new HBox(submitButton, continueButton);

    public LessonView(final LessonModel model, final LessonController controller) {
        this.model = model;
        this.controller = controller;
        addChildren();
        bindProperties();
        registerActions();
        prettify();
    }

    private void addChildren() {
        getChildren().addAll(
            statusText,
            scoreText,
            wordLabel,
            answerField,
            gradeText,
            remarksText,
            buttonPanel,
            finishButton
        );
    }

    private void prettify() {
        configureLayout();
        wordLabel.setFont(new Font(32));
        answerField.setFont(new Font(24));
        answerField.setMinWidth(440);
        answerField.setAlignment(Pos.CENTER);
        finishButton.setFont(new Font(18));
    }

    private void configureLayout() {
        buttonPanel.setSpacing(20);
        buttonPanel.setAlignment(Pos.CENTER);
        setSpacing(20);
        setAlignment(Pos.CENTER);
        answerField.setMaxWidth(300);
    }

    private void bindProperties() {
        bindDisplayControls();
        bindInputControls();
    }

    private void bindDisplayControls() {
        statusText.textProperty().bind(model.status);
        scoreText.textProperty().bind(model.score);
        wordLabel.textProperty().bind(model.wordToTranslate);
        gradeText.textProperty().bind(model.gradeDescription);
        gradeText.fillProperty().bind(model.gradeColor);
        remarksText.textProperty().bind(model.remarksText);
    }

    private void bindInputControls() {
        answerField.editableProperty().bind(model.quizzing);
        answerField.visibleProperty().bind(model.inProgress);
        submitButton.defaultButtonProperty().bind(model.quizzing);
        submitButton.disableProperty().bind(model.quizzing.not());
        submitButton.visibleProperty().bind(model.inProgress);
        continueButton.defaultButtonProperty().bind(model.quizzing.not());
        continueButton.disableProperty().bind(model.quizzing);
        continueButton.visibleProperty().bind(model.inProgress);
        finishButton.defaultButtonProperty().bind(model.inProgress.not());
    }

    private void registerActions() {
        submitButton.setOnAction(this::submitButtonOnAction);
        continueButton.setOnAction(this::continueButtonOnAction);
        finishButton.setOnAction(this::finishButtonOnAction);
    }

    private void finishButtonOnAction(final ActionEvent actionEvent) {
        controller.finishAction(StageUtils.getStage(actionEvent));
    }

    private void submitButtonOnAction(final ActionEvent ignored) {
        controller.submitAction(answerField.getText());
    }

    private void continueButtonOnAction(final ActionEvent ignored) {
        answerField.setText(null);
        controller.continueAction();
    }
}
