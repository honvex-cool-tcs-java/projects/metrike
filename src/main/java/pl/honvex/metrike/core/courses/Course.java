package pl.honvex.metrike.core.courses;

import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.core.learning.judges.Judge;
import pl.honvex.metrike.core.learning.judges.StrictJudge;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Course implements Serializable {
    private final List<Flashcard> flashcards;
    private Judge judge;
    private String name;

    public Course(final List<Flashcard> flashcards, final String name) {
        this.flashcards = flashcards;
        judge = new StrictJudge();
        this.name = name;
    }

    public Course(final List<Flashcard> flashcards) {
        this(flashcards, "unnamed course");
    }

    public Course() {
        this(new ArrayList<>());
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Flashcard> getFlashcards() {
        return flashcards;
    }

    public Lesson nextLesson(final int size) {
        final var asList = new ArrayList<>(flashcards);
        Collections.shuffle(asList);
        return new Lesson(asList.stream().limit(size).toList(), judge);
    }

    public static Course fromString(final String representation) {
        return new Course(
            Arrays.stream(representation.split("\n"))
                .filter(line -> !line.isEmpty())
                .map(Flashcard::fromString)
                .collect(Collectors.toList())
        );
    }

    public static Course fromFile(final File file) throws IOException {
        if(!file.getName().endsWith(".dict"))
            throw new IOException("Please provide a .dict file");
        final var course = fromString(new Scanner(file).useDelimiter("\\Z").next());
        course.setName(
            file.getName()
                .replace(".dict", "")
                .replace("_", " ")
        );
        return course;
    }

    @Override
    public String toString() {
        return getName() + " (" + flashcards.size() + " flashcards)";
    }

    public boolean isEmpty() {
        return getFlashcards().isEmpty();
    }

    public void setJudge(final Judge judge) {
        this.judge = judge;
    }

    public Judge getJudge() {
        return judge;
    }
}
