package pl.honvex.metrike.core.courses;

import pl.honvex.metrike.core.learning.Flashcard;
import pl.honvex.metrike.core.learning.grades.Grade;
import pl.honvex.metrike.core.learning.grades.Quality;
import pl.honvex.metrike.core.learning.judges.Judge;

import java.util.Collection;
import java.util.Iterator;

public class Lesson {
    private final Iterator<Flashcard> flashcards;
    private final Judge judge;
    private final int questionCount;
    private int questionNumber = 0;
    private int score = 0;

    public Lesson(final Collection<Flashcard> flashcards, final Judge judge) {
        this.flashcards = flashcards.iterator();
        questionCount = flashcards.size();
        this.judge = judge;
    }

    public Question nextQuestion() {
        return new Question(++questionNumber, flashcards.next());
    }

    public boolean isInProgress() {
        return flashcards.hasNext();
    }

    public int getScore() {
        return score;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public class Question {
        public final int number;

        private final Flashcard flashcard;

        Question(final int number, final Flashcard flashcard) {
            this.flashcard = flashcard;
            this.number = number;
        }

        public String get() {
            return flashcard.getWord();
        }

        public Grade answer(final String providedAnswer) {
            final var grade = flashcard.gradeAnswer(providedAnswer, judge);
            if(grade.quality() != Quality.WRONG)
                ++score;
            return grade;
        }
    }
}
