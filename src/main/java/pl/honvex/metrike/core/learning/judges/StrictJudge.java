package pl.honvex.metrike.core.learning.judges;

import pl.honvex.metrike.core.learning.grades.BinaryGrade;
import pl.honvex.metrike.core.learning.grades.Grade;

public class StrictJudge extends SensibleJudge {
    @Override
    protected Grade gradeNormalized(final String provided, final String expected, final String ignored) {
        return new BinaryGrade(provided, expected);
    }

    @Override
    public String toString() {
        return "Strict";
    }
}
