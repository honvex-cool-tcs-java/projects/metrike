package pl.honvex.metrike.core.learning.judges;

import pl.honvex.metrike.core.learning.grades.BinaryGrade;
import pl.honvex.metrike.core.learning.grades.Grade;

public class SuperStrictJudge implements Judge {
    @Override
    public Grade gradeTranslation(String provided, String expected, String word) {
        return new BinaryGrade(provided, expected);
    }

    @Override
    public String toString() {
        return "Super Strict";
    }
}
