package pl.honvex.metrike.core.learning.grades;

public enum Quality {
    PERFECT,
    INCORRECT_CAPITALIZATION,
    INCORRECT_DIACRITICS,
    TYPO,
    PARTIALLY_INCORRECT,
    WRONG
}
