package pl.honvex.metrike.core.learning.grades;

public interface Grade {
    Quality quality();

    String remarks();
}
