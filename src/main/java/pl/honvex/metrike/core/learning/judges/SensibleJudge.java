package pl.honvex.metrike.core.learning.judges;

import pl.honvex.metrike.core.learning.grades.Grade;
import pl.honvex.metrike.core.utils.TextUtils;

public abstract class SensibleJudge implements Judge {
    @Override
    public final Grade gradeTranslation(final String provided, final String expected, final String word) {
        return gradeNormalized(TextUtils.normalize(provided), expected, word);
    }

    protected abstract Grade gradeNormalized(String provided, String expected, String word);
}
