package pl.honvex.metrike.core.learning.judges;

import pl.honvex.metrike.core.learning.grades.Grade;
import pl.honvex.metrike.core.learning.grades.Quality;
import pl.honvex.metrike.core.learning.grades.SimpleGrade;

public class PermissiveJudge extends SensibleJudge {
    @Override
    protected Grade gradeNormalized(String provided, String expected, String word) {
        if(provided.equals(expected))
            return new SimpleGrade(Quality.PERFECT, null);
        final var generalMessage = "\"" + provided + "\" should be \"" + expected + "\"";
        if(provided.equalsIgnoreCase(expected))
            return new SimpleGrade(
                Quality.INCORRECT_CAPITALIZATION,
                "Pay attention to capitalization: " + generalMessage
            );
        return new SimpleGrade(Quality.WRONG, generalMessage);
    }

    @Override
    public String toString() {
        return "Permissive";
    }
}
