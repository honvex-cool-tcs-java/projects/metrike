package pl.honvex.metrike.core.learning.grades;

public class SimpleGrade implements Grade {
    protected Quality quality;
    protected String remarks;

    public SimpleGrade() {
    }

    public SimpleGrade(Quality quality, String remarks) {
        this.quality = quality;
        this.remarks = remarks;
    }

    @Override
    public Quality quality() {
        return quality;
    }

    @Override
    public String remarks() {
        return remarks;
    }
}
