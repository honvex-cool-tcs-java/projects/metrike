package pl.honvex.metrike.core.learning;

import pl.honvex.metrike.core.learning.grades.Grade;
import pl.honvex.metrike.core.learning.judges.Judge;
import pl.honvex.metrike.core.utils.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class Flashcard implements Serializable {
    private String word;
    private final List<String> translations;

    public Flashcard() {
        this("", new ArrayList<>());
    }

    public Flashcard(String word, List<String> translations) {
        if(word == null)
            throw new IllegalArgumentException("`word` must not be `null`");
        if(translations == null)
            throw new IllegalArgumentException("`translations` must not be `null`");
        this.word = word;
        this.translations = translations;
    }

    public Grade gradeAnswer(final String provided, final Judge judge) {
        return translations.stream()
            .map(translation -> judge.gradeTranslation(provided, translation, word))
            .min(Comparator.comparing(Grade::quality))
            .orElse(null);
    }

    @Override
    public String toString() {
        return word + ":" + String.join("|", translations);
    }

    public static Flashcard fromString(final String representation) {
        final var parts = representation.split(":");
        return new Flashcard(
            parts[0].trim(),
            Arrays.stream(parts[1].split("\\|"))
                .map(TextUtils::normalize)
                .collect(Collectors.toList())
        );
    }

    public String getWord() {
        return word;
    }

    public void setWord(final String word) {
        this.word = word;
    }

    public List<String> getTranslations() {
        return translations;
    }
}
