package pl.honvex.metrike.core.learning.judges;

import pl.honvex.metrike.core.learning.grades.Grade;

import java.io.Serializable;

public interface Judge extends Serializable {
    Grade gradeTranslation(String provided, String expected, String word);
}
