package pl.honvex.metrike.core.learning.grades;

public class BinaryGrade extends SimpleGrade {

    public BinaryGrade(final String provided, final String expected) {
        if(provided.equals(expected)) {
            quality = Quality.PERFECT;
            remarks = null;
        } else {
            quality = Quality.WRONG;
            remarks = "\"" + provided + "\" should be \"" + expected + "\"";
        }
    }
}
