package pl.honvex.metrike.core.utils;

public final class TextUtils {
    private TextUtils() {
    }

    public static String normalize(final String string) {
        return string.replaceAll(" +", " ").trim();
    }

    public static boolean isEffectivelyEmpty(final String string) {
        return string == null || string.isBlank();
    }
}
