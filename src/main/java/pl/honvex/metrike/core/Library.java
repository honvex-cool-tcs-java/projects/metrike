package pl.honvex.metrike.core;

import pl.honvex.metrike.Config;
import pl.honvex.metrike.core.courses.Course;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Library implements Serializable {
    private final List<Course> courses;

    private Library(final List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public static Library fromDirectory(final String path) throws IOException {
        return fromDirectory(new File(path));
    }

    public static Library fromDirectory(final File directory) throws IOException {
        final var files = directory.listFiles(
            (ignored, name) -> name.endsWith(Config.DICTIONARY_FILE_EXTENSION)
        );
        if(files == null)
            throw new IOException();
        final var courses = new ArrayList<Course>();
        for(final var file : files)
            courses.add(Course.fromFile(file));
        return new Library(courses);
    }
}
