package pl.honvex.metrike;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.honvex.metrike.core.Library;
import pl.honvex.metrike.gui.Scenes;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Metrike extends Application {
    @Override
    public void start(final Stage stage) {
        final var library = getLibrary();
        if(library == null)
            return;
        stage.setOnCloseRequest(ignored -> saveLibrary(library));
        stage.setMinWidth(Config.MIN_WINDOW_WIDTH);
        stage.setMinHeight(Config.MIN_WINDOW_HEIGHT);
        stage.setScene(Scenes.browseCoursesScene(library));
        stage.show();
    }

    private void saveLibrary(final Library library) {
        try(
            final var stream = new FileOutputStream(Config.LIBRARY_PATH);
            final var out = new ObjectOutputStream(stream)
        ) {
            out.writeObject(library);
        } catch(final Exception exception) {
            System.err.println("Sorry, the library could not be saved:");
            exception.printStackTrace();
        }
    }

    private Library getLibrary() {
        try {
            return deserializeLibrary();
        } catch(final Exception ignored) {
            try {
                System.err.println("Attempting to load default library");
                return Library.fromDirectory(Config.DEFAULT_DICTIONARY_LOCATION);
            } catch(final Exception exception) {
                System.err.println("Sorry, cannot load library :(");
                return null;
            }
        }
    }

    private Library deserializeLibrary() throws Exception {
        try(
            final var stream = new FileInputStream(Config.LIBRARY_PATH);
            final var in = new ObjectInputStream(stream)
        ) {
            return (Library)in.readObject();
        } catch(final Exception exception) {
            System.err.println("Sorry, cannot deserialize library");
            throw exception;
        }
    }
}
