package pl.honvex.metrike;

import pl.honvex.metrike.core.learning.judges.Judge;
import pl.honvex.metrike.core.learning.judges.PermissiveJudge;
import pl.honvex.metrike.core.learning.judges.StrictJudge;
import pl.honvex.metrike.core.learning.judges.SuperStrictJudge;

import java.util.Arrays;
import java.util.List;

public final class Config {

    private Config() {
    }

    public static final String DICTIONARY_FILE_EXTENSION = ".dict";
    public static final String DEFAULT_DICTIONARY_LOCATION = "resources/dictionaries";
    public static final String LIBRARY_PATH = "library.serialized";
    public static final int MIN_WINDOW_WIDTH = 640;
    public static final int MIN_WINDOW_HEIGHT = 480;

    public static final List<Judge> AVAILABLE_JUDGES = Arrays.asList(
        new StrictJudge(),
        new PermissiveJudge(),
        new SuperStrictJudge()
    );
}
