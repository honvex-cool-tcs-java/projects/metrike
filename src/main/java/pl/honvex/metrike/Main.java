package pl.honvex.metrike;

import javafx.application.Application;

public class Main {
    public static void main(final String[] args) {
        Application.launch(Metrike.class, args);
    }
}
