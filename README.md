# Metrike

Metrike is a flashcard app written in Java using JavaFX.

The user can provide their own dictionary file and use this app to learn and revise words.

### The name

The name of this app should be pronounced "mee-tree-kee".

The word "mētrikḗ" is a transliteration of the Greek word "μητρική" which is used in the phrase "η
μητρική γλώσσα" meaning "the mother tongue".
